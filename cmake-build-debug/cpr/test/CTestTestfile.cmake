# CMake generated Testfile for 
# Source directory: /root/test/c++/cpr_test/cpr/test
# Build directory: /root/test/c++/cpr_test/cmake-build-debug/cpr/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(cpr_get_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/get_tests")
add_test(cpr_post_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/post_tests")
add_test(cpr_session_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/session_tests")
add_test(cpr_async_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/async_tests")
add_test(cpr_proxy_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/proxy_tests")
add_test(cpr_head_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/head_tests")
add_test(cpr_delete_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/delete_tests")
add_test(cpr_put_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/put_tests")
add_test(cpr_callback_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/callback_tests")
add_test(cpr_raw_body_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/raw_body_tests")
add_test(cpr_options_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/options_tests")
add_test(cpr_patch_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/patch_tests")
add_test(cpr_error_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/error_tests")
add_test(cpr_alternating_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/alternating_tests")
add_test(cpr_util_tests "/root/test/c++/cpr_test/cmake-build-debug/bin/util_tests")
