# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/test/c++/cpr_test/cpr/cpr/auth.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/auth.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/cookies.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/cookies.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/cprtypes.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/cprtypes.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/digest.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/digest.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/error.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/error.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/multipart.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/multipart.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/parameters.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/parameters.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/payload.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/payload.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/proxies.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/proxies.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/session.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/session.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/ssl_options.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/ssl_options.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/timeout.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/timeout.cpp.o"
  "/root/test/c++/cpr_test/cpr/cpr/util.cpp" "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/util.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_CPR_TESTS"
  "CMAKE_USE_OPENSSL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../cpr/include"
  "../cpr/opt/curl/include"
  "cpr/opt/curl/include/curl"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/test/c++/cpr_test/cmake-build-debug/cpr/opt/curl/lib/CMakeFiles/libcurl.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
