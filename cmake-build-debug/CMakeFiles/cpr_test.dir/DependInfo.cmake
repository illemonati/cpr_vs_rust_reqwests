# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/test/c++/cpr_test/main.cpp" "/root/test/c++/cpr_test/cmake-build-debug/CMakeFiles/cpr_test.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../cpr/include"
  "/usr/include/x86_64-linux-gnu"
  "../cpr/opt/curl/include"
  "cpr/opt/curl/include/curl"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/test/c++/cpr_test/cmake-build-debug/cpr/cpr/CMakeFiles/cpr.dir/DependInfo.cmake"
  "/root/test/c++/cpr_test/cmake-build-debug/cpr/opt/curl/lib/CMakeFiles/libcurl.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
