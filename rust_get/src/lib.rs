use reqwest;
use std::ffi::CStr;

type c_char = i8;
#[no_mangle]
pub extern "C" fn rget(url: *const c_char) -> *const c_char {
    let url: &CStr = unsafe { CStr::from_ptr(url) };
    let url: &str = url.to_str().unwrap();
    println!("rust getting {}", &url);
    let mut r = reqwest::get(url).unwrap();
    let rt = r.text().unwrap();
    let crt = rt.as_str().as_ptr() as *const c_char;
    return crt;
}
