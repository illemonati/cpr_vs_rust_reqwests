#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <new>

extern "C" {

const char *rget(const char *url);

} // extern "C"
