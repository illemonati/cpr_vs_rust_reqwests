#include <cpr/cpr.h>
#include "rust_get/rust_get.h"
#include <iostream>



int main(int argc, char** argv) {
    std::cout << rget((char *)"https://www.tioft.tech") << std::endl;
    auto r = cpr::Get(cpr::Url("https://www.tioft.tech"));
    std::cout << r.text << std::endl;
}